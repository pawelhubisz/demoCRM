package com.example.demo.service;

import com.example.demo.exception.CRMException;
import com.example.demo.model.Account;
import com.example.demo.request.AccountRequestBody;

import java.util.List;

public interface AccountService {
    void saveAccount(AccountRequestBody account) throws CRMException;

    List<Account> getAccountList();

    List<Account> getDeletedAccountList();

    List<Account> getVipAccountList();

    List<Account> getNotVipAndNotDeletedAccountList();

    List<Account> getAccountWithProfileList();

    Account getAccountByLogin(String login) throws CRMException;

}
