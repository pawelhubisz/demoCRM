package com.example.demo.controllers;

import com.example.demo.exception.CRMException;
import com.example.demo.service.DocumentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class DocumentController {
    private DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    List<Integer> documentTypeList= Arrays.asList(1,3,4);

    @RequestMapping(value = "/documents")
    public String getAllDocuments(Model model) throws CRMException {
        for(Integer documentType:documentTypeList){
            addAttributeDocumentType(documentType,model);
        }
        return "documents";
    }

    private void addAttributeDocumentType(int type, Model model) throws CRMException {
            model.addAttribute("documentType"+type, documentService.getDocumentsByTypeList(type));
    }
}
