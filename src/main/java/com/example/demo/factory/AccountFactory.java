package com.example.demo.factory;

import com.example.demo.model.Account;
import com.example.demo.request.AccountRequestBody;

public class AccountFactory {
    public static Account createAccount(AccountRequestBody accountRequestBody) {
        Account account = new Account();
        account.setName(accountRequestBody.getName());
        account.setLogin(accountRequestBody.getLogin());
        account.setEmail(accountRequestBody.getEmail());
        return account;
    }
}
