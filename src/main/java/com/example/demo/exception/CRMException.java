package com.example.demo.exception;

public class CRMException extends Exception{

    public CRMException(String message) {
        super(message);
    }
}
