package com.example.demo.service.impl;


import com.example.demo.exception.CRMException;
import com.example.demo.model.Account;
import com.example.demo.repository.AccountRepository;
import com.example.demo.request.AccountRequestBody;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class AccountServiceImplTest {

    @InjectMocks
    private AccountServiceImpl accountServiceImpl;
    @Mock
    private AccountRepository accountRepository;

    @Test
    public void testGetAccountByLoginShouldReturnAccount() throws Exception {
        //GIVEN
        String login = "TestLogin";
        Account expectedAccount = new Account();
        expectedAccount.setLogin(login);
        //WHEN
        when(accountRepository.getAccountByLogin(login)).thenReturn(expectedAccount);
        Account account = accountServiceImpl.getAccountByLogin(login);
        //THEN
        assertNotNull(account);
        assertEquals(login, account.getLogin());
    }

    @Test
    public void testGetAccountByLoginShouldReturnNull() throws Exception {
        //GIVEN
        String login = "TestLogin";
        //WHEN
        when(accountRepository.getAccountByLogin(login)).thenReturn(null);
        Account account = accountServiceImpl.getAccountByLogin(login);
        //THEN
        assertNull(account);
    }

    @Test
    public void testGetAccountByLoginShouldThrowCRMExceptionFromSQLException() throws SQLException {
        //GIVEN
        String login = "TestLogin";
        //WHEN
        when(accountRepository.getAccountByLogin(login)).thenThrow(new SQLException());
        //THEN
        CRMException crmException = assertThrows(CRMException.class, () -> accountServiceImpl.getAccountByLogin(login));
        assertEquals("Błąd bazy danych", crmException.getMessage());
    }

    @Test
    public void testGetAccountByLoginShouldThrowCRMExceptionFromRuntime() throws SQLException {
        //GIVEN
        String login = "TestLogin";
        //WHEN
        when(accountRepository.getAccountByLogin(login)).thenThrow(new RuntimeException());
        //THEN
        CRMException crmException = assertThrows(CRMException.class, () -> accountServiceImpl.getAccountByLogin(login));
        assertEquals("Nieznany błąd", crmException.getMessage());
    }

    @Test
    public void testSaveAccountShouldReturnAccount() throws Exception {
        //GIVEN
        //WHEN
        when(accountRepository.save(any(Account.class))).thenReturn(null);
        accountServiceImpl.saveAccount(new AccountRequestBody());
        //THEN
    }
    @Test
    public void testSaveAccountShouldThrowException() throws Exception {
        //GIVEN
        //WHEN
        when(accountRepository.save(any(Account.class))).thenThrow(new RuntimeException());
        //THEN
        CRMException crmException = assertThrows(CRMException.class, () -> accountServiceImpl.saveAccount(new AccountRequestBody()));
        assertEquals("Nieznany błąd", crmException.getMessage());
    }
}