package com.example.demo.factory;

import com.example.demo.model.Document;
import com.example.demo.request.DocumentRequestBody;
import com.example.demo.service.DocumentService;

import java.util.Date;

public class DocumentFactory {

    public static Document createDocument(DocumentRequestBody documentRequestBody) {
        Document document = new Document();
        document.setAuthor(documentRequestBody.getAuthor());
        document.setNumber(documentRequestBody.getNumber());
        document.setDocumentId(documentRequestBody.getDocumentId());
        document.setComment(documentRequestBody.getComment());
        document.setType(documentRequestBody.getType());
        document.setAddDate(documentRequestBody.getAddDate());
        return document;
    }

}
