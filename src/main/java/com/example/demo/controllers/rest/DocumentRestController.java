package com.example.demo.controllers.rest;

import com.example.demo.exception.CRMException;
import com.example.demo.model.Account;
import com.example.demo.model.Document;
import com.example.demo.request.AccountRequestBody;
import com.example.demo.request.DocumentRequestBody;
import com.example.demo.service.AccountService;
import com.example.demo.service.DocumentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DocumentRestController {
    private DocumentService documentService;

    public DocumentRestController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping("/document")
    public ResponseEntity getDocumentByNumber(String number) {
        Document document = null;
        try {
            document = documentService.getDocumentByNumber(number);
        } catch (CRMException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        if (document == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Brak numeru dokumentu w bazie");
        }
        return new ResponseEntity(document, HttpStatus.OK);
    }
    @PostMapping("/document")
    public ResponseEntity saveDocument(@RequestBody DocumentRequestBody documentRequestBody) {
        try {
            documentService.saveDocument(documentRequestBody);
        } catch (CRMException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());

        }
        return new ResponseEntity(HttpStatus.OK);
    }


}
