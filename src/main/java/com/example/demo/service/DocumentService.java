package com.example.demo.service;

import com.example.demo.exception.CRMException;
import com.example.demo.model.Document;
import com.example.demo.request.DocumentRequestBody;

import java.util.List;

public interface DocumentService {
    void saveDocument(DocumentRequestBody document) throws CRMException;

    Document getDocumentByNumber(String number) throws CRMException;

    List<Document> getDocumentsByTypeList(int type) throws CRMException;



}
