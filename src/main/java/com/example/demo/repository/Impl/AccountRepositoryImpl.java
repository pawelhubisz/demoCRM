package com.example.demo.repository.Impl;

import com.example.demo.model.Account;
import com.example.demo.repository.AccountRepository;
import com.example.demo.repository.AccountRepositoryCustom;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.SQLException;
import java.util.List;

@Repository
public class AccountRepositoryImpl implements AccountRepositoryCustom {

    private EntityManager em;

    public AccountRepositoryImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<Account> getAccountWithNoProfileList() {

        return null;
    }

    @Override
    public Account getAccountByLogin(String login) throws SQLException {
       // throw new SQLException("Błąd bazy");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);

        Root<Account> account = cq.from(Account.class);
        Predicate loginPredicate = cb.equal(account.get("login"), login);
        cq.where(loginPredicate);

        TypedQuery<Account> query = em.createQuery(cq);
        return query.getResultList().stream().findFirst().orElse(null);
    }
}
