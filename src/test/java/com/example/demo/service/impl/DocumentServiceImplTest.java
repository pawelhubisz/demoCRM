package com.example.demo.service.impl;

import com.example.demo.exception.CRMException;
import com.example.demo.model.Account;
import com.example.demo.model.Document;
import com.example.demo.repository.AccountRepository;
import com.example.demo.repository.DocumentRepository;
import com.example.demo.request.AccountRequestBody;
import com.example.demo.request.DocumentRequestBody;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import javax.print.Doc;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class DocumentServiceImplTest {
    @InjectMocks
    private DocumentServiceImpl documentServiceImpl;
    @Mock
    private DocumentRepository documentRepository;

    @Test
    public void testGetDocumentByNumberShouldReturnDocument() throws Exception {
        //GIVEN
        String number = "1";
        Document expectedDocument = new Document();
        expectedDocument.setNumber(number);
        //WHEN
        when(documentRepository.getDocumentByNumber(number)).thenReturn(expectedDocument);
        Document document = documentServiceImpl.getDocumentByNumber(number);
        //THEN
        assertNotNull(document);
        assertEquals(number, document.getNumber());
    }

    @Test
    public void testGetDocumentByNumberShouldReturnNull() throws Exception {
        //GIVEN
        String number = "1";
        //WHEN
        when(documentRepository.getDocumentByNumber(number)).thenReturn(null);
        Document document = documentServiceImpl.getDocumentByNumber(number);
        //THEN
        assertNull(document);
    }
    @Test
    public void testGetDocumentByNumberShouldThrowCRMExceptionFromSQLException() throws SQLException {
        //GIVEN
        String number = "1";
        //WHEN
        when(documentRepository.getDocumentByNumber(number)).thenThrow(new SQLException());
        //THEN
        CRMException crmException = assertThrows(CRMException.class, () -> documentServiceImpl.getDocumentByNumber(number));
        assertEquals("Błąd bazy danych", crmException.getMessage());
    }
    @Test
    public void testGetDocumentByNumberShouldThrowCRMExceptionFromRuntime() throws SQLException {
        //GIVEN
        String number = "1";
        //WHEN
        when(documentRepository.getDocumentByNumber(number)).thenThrow(new RuntimeException());
        //THEN
        CRMException crmException = assertThrows(CRMException.class, () -> documentServiceImpl.getDocumentByNumber(number));
        assertEquals("Nieznany błąd", crmException.getMessage());
    }
    @Test
    public void testSaveDocumentShouldReturnDocument() throws Exception {
        //GIVEN
        //WHEN
        when(documentRepository.save(any(Document.class))).thenReturn(null);
        documentServiceImpl.saveDocument(new DocumentRequestBody());
        //THEN
    }
    @Test
    public void testSaveDocumentShouldThrowException() throws Exception {
        //GIVEN
        //WHEN
        when(documentRepository.save(any(Document.class))).thenThrow(new RuntimeException());
        //THEN
        CRMException crmException = assertThrows(CRMException.class, () -> documentServiceImpl.saveDocument(new DocumentRequestBody()));
        assertEquals("Nieznany błąd", crmException.getMessage());
    }

    @Test
    public void testGetDocumentsByTypeListShouldReturnDocument() throws Exception {
        //GIVEN
        int type = 1;
        Document expectedDocument = new Document();
        expectedDocument.setType(type);
        List<Document> expectedDocumentList= new ArrayList<>();
        expectedDocumentList.add(expectedDocument);
        //WHEN
        when(documentRepository.getDocumentsByTypeList(type)).thenReturn(expectedDocumentList);
        List<Document> documentList = documentServiceImpl.getDocumentsByTypeList(type);
        //THEN
        assertNotNull(documentList);
        assertEquals(expectedDocumentList.size(), documentList.size());
        assertEquals(expectedDocumentList.get(0).getType(), documentList.get(0).getType());
    }

    @Test
    public void testGetDocumentsByTypeListShouldReturnNull() throws Exception {
        //GIVEN
        int type = 1;
        Document expectedDocument = new Document();
        expectedDocument.setType(type);
        List<Document> expectedDocumentList= new ArrayList<>();
        expectedDocumentList.add(expectedDocument);
        //WHEN
        when(documentRepository.getDocumentsByTypeList(type)).thenReturn(null);
        List<Document> documentList = documentServiceImpl.getDocumentsByTypeList(type);
        //THEN
        assertNull(documentList);
    }
    @Test
    public void testGetDocumentsByTypeListShouldThrowCRMExceptionFromSQLException() throws SQLException {
        //GIVEN
        int type = 1;
        Document expectedDocument = new Document();
        expectedDocument.setType(type);
        List<Document> expectedDocumentList= new ArrayList<>();
        expectedDocumentList.add(expectedDocument);
        //WHEN
        when(documentRepository.getDocumentsByTypeList(type)).thenThrow(new SQLException());
        //THEN
        CRMException crmException = assertThrows(CRMException.class, () -> documentServiceImpl.getDocumentsByTypeList(type));
        assertEquals("Błąd bazy danych", crmException.getMessage());
    }
    @Test
    public void testGetDocumentsByTypeListShouldThrowCRMExceptionFromRuntime() throws SQLException {
        //GIVEN
        int type = 1;
        Document expectedDocument = new Document();
        expectedDocument.setType(type);
        List<Document> expectedDocumentList= new ArrayList<>();
        expectedDocumentList.add(expectedDocument);
        //WHEN
        when(documentRepository.getDocumentsByTypeList(type)).thenThrow(new RuntimeException());
        //THEN
        CRMException crmException = assertThrows(CRMException.class, () -> documentServiceImpl.getDocumentsByTypeList(type));
        assertEquals("Nieznany błąd", crmException.getMessage());
    }

}
