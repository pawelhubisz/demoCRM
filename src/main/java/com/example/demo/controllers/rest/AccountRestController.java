package com.example.demo.controllers.rest;

import com.example.demo.exception.CRMException;
import com.example.demo.model.Account;
import com.example.demo.request.AccountRequestBody;
import com.example.demo.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AccountRestController {
    private AccountService accountService;

    public AccountRestController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/accountlist")
    public List<Account> getAccountList() {
        return accountService.getAccountList();
    }

    @GetMapping("/account")
    public ResponseEntity getAccount(String login) {
        Account account = null;
        try {
            account = accountService.getAccountByLogin(login);
        } catch (CRMException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        if (account == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Brak użytkownika w bazie");
        }
        return new ResponseEntity(account, HttpStatus.OK);
    }

    @PostMapping("/account")
    public ResponseEntity saveAccount(@RequestBody AccountRequestBody accountRequestBody) {
        try {
            accountService.saveAccount(accountRequestBody);
        } catch (CRMException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());

        }
        return new ResponseEntity(HttpStatus.OK);
    }

}