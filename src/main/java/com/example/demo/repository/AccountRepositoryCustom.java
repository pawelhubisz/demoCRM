package com.example.demo.repository;

import com.example.demo.model.Account;

import java.sql.SQLException;
import java.util.List;

public interface AccountRepositoryCustom {

    List<Account> getAccountWithNoProfileList();

    Account getAccountByLogin(String login) throws SQLException;
}
