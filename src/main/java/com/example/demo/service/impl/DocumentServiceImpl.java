package com.example.demo.service.impl;

import com.example.demo.exception.CRMException;
import com.example.demo.factory.DocumentFactory;
import com.example.demo.model.Account;
import com.example.demo.model.Document;
import com.example.demo.repository.AccountRepository;
import com.example.demo.repository.DocumentRepository;
import com.example.demo.request.DocumentRequestBody;
import com.example.demo.service.DocumentService;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService {

    private DocumentRepository documentRepository;

    public DocumentServiceImpl(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }


    @Override
    public void saveDocument(DocumentRequestBody documentRequestBody) throws CRMException {
        Document document = DocumentFactory.createDocument(documentRequestBody);
        try {
            documentRepository.save(document);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CRMException("Nieznany błąd");
        }
    }


    @Override
    public Document getDocumentByNumber(String number) throws CRMException {
        try {
            return documentRepository.getDocumentByNumber(number);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CRMException("Błąd bazy danych");
        } catch (Exception e) {
            e.printStackTrace();
            throw new CRMException("Nieznany błąd");
        }
    }

    @Override
    public List<Document> getDocumentsByTypeList(int type) throws CRMException {
        try {
            return documentRepository.getDocumentsByTypeList(type);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CRMException("Błąd bazy danych");
        } catch (Exception e) {
            e.printStackTrace();
            throw new CRMException("Nieznany błąd");
        }
    }

}
