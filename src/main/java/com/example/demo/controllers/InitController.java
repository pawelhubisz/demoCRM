package com.example.demo.controllers;

import com.example.demo.model.Account;
import com.example.demo.model.Profile;
import com.example.demo.service.AccountService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//TODO do wywalenia cała klasa po zrobieniu ładowania sql na start apki
@RestController
public class InitController {
    private AccountService accountService;

    public InitController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/setup")
    public String setUp() {
        return "Data saved";
    }


}


