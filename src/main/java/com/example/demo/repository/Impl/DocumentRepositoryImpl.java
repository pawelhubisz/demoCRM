package com.example.demo.repository.Impl;

import com.example.demo.model.Account;
import com.example.demo.model.Document;
import com.example.demo.repository.DocumentRepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.SQLException;
import java.util.List;

@Repository
public class DocumentRepositoryImpl implements DocumentRepositoryCustom {
    private EntityManager em;

    public DocumentRepositoryImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Document getDocumentByNumber(String number) throws SQLException {
        // throw new SQLException("Błąd bazy");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Document> cq = cb.createQuery(Document.class);

        Root<Document> document = cq.from(Document.class);
        Predicate loginPredicate = cb.equal(document.get("number"), number);
        cq.where(loginPredicate);

        TypedQuery<Document> query = em.createQuery(cq);
        return query.getSingleResult();
    }

    @Override
    public List<Document> getDocumentsByTypeList(int type) throws SQLException {
// throw new SQLException("Błąd bazy");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Document> cq = cb.createQuery(Document.class);

        Root<Document> document = cq.from(Document.class);
        Predicate typePredicate = cb.equal(document.get("type"), type);
        cq.where(typePredicate);

        TypedQuery<Document> query = em.createQuery(cq);
        return query.getResultList();
    }
}
