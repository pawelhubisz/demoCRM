package com.example.demo.repository;

import com.example.demo.model.Document;

import java.sql.SQLException;
import java.util.List;

public interface DocumentRepositoryCustom  {
    Document getDocumentByNumber(String name) throws SQLException;

    List<Document> getDocumentsByTypeList(int type)throws SQLException;
}
